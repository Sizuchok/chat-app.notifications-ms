import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { GmailModule } from './providers/gmail/gmail.module';
import { NotificationsModule } from './notifications/notifications.module';

@Module({
  imports: [
    ConfigModule.forRoot({ isGlobal: true }),
    GmailModule,
    NotificationsModule,
  ],
})
export class AppModule {}
