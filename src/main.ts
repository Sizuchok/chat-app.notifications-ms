import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { MicroserviceOptions, Transport } from '@nestjs/microservices';
import { ValidationPipe } from '@nestjs/common';
import {
  NOTIFICATIONS_MS_CLIENT_ID,
  NOTIFICATIONS_MS_CONSUMER_ID,
} from './common/const/kafka/kafka-ms-config.const';

async function bootstrap() {
  const app = await NestFactory.createMicroservice<MicroserviceOptions>(
    AppModule,
    {
      transport: Transport.KAFKA,
      options: {
        client: {
          brokers: [process.env.KAFKA_BROKER_URL],
          clientId: NOTIFICATIONS_MS_CLIENT_ID,
        },
        consumer: { groupId: NOTIFICATIONS_MS_CONSUMER_ID },
      },
    },
  );

  app.useGlobalPipes(
    new ValidationPipe({
      whitelist: true,
      transform: true,
    }),
  );

  await app.listen();
}
bootstrap();
