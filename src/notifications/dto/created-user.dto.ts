import { IsEmail, IsString } from 'class-validator';

export class CreatedUserDataDto {
  @IsString()
  name: string;

  @IsEmail()
  email: string;

  @IsString()
  verificationLink: string;
}
