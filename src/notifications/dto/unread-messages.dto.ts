import { ArrayNotEmpty, IsEmail, IsNotEmpty, IsNumber } from 'class-validator';

export class UnreadMessagesDto {
  @IsNotEmpty()
  chatName: string;

  @IsNumber()
  numberOfMessages: number;

  @ArrayNotEmpty()
  @IsEmail({}, { each: true })
  emails: string[];
}
