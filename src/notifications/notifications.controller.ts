import { Controller } from '@nestjs/common';
import { EventPattern, MessagePattern, Payload } from '@nestjs/microservices';
import { NotificationsService } from './notifications.service';
import { CreatedUserDataDto } from './dto/created-user.dto';
import { USER } from '../common/const/user-events.const';
import { CHAT } from '../common/const/chat-events.const';
import { UnreadMessagesDto } from './dto/unread-messages.dto';

@Controller()
export class NotificationsController {
  constructor(private readonly notificationsService: NotificationsService) {}

  @MessagePattern(USER.NEW)
  sendVerificationEmail(@Payload() data: CreatedUserDataDto) {
    return this.notificationsService.sendVerificationEmail(data);
  }

  @EventPattern(CHAT.NEW_UNREAD_MESSAGES)
  sendUnreadMessagesEmail(@Payload() data: UnreadMessagesDto) {
    return this.notificationsService.sendUnreadMessagesEmail(data);
  }
}
