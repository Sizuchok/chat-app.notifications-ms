import { Module } from '@nestjs/common';
import { NotificationsService } from './notifications.service';
import { NotificationsController } from './notifications.controller';
import { GmailModule } from '../providers/gmail/gmail.module';

@Module({
  controllers: [NotificationsController],
  providers: [NotificationsService],
  imports: [GmailModule],
})
export class NotificationsModule {}
