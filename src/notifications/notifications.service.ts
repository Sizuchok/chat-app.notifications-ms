import { Injectable } from '@nestjs/common';
import { CreatedUserDataDto } from './dto/created-user.dto';
import { GmailService } from '../providers/gmail/gmail.service';
import { UnreadMessagesDto } from './dto/unread-messages.dto';
import { ConfigService } from '@nestjs/config';

@Injectable()
export class NotificationsService {
  constructor(
    private readonly gmailService: GmailService,
    private readonly configService: ConfigService,
  ) {}

  async sendVerificationEmail({ email, ...data }: CreatedUserDataDto) {
    await this.gmailService.sendTemplatedEmail(
      email,
      'Verify your account',
      'sign-up-email',
      data,
    );
  }

  async sendUnreadMessagesEmail({
    chatName,
    numberOfMessages,
    emails,
  }: UnreadMessagesDto) {
    if (this.configService.get('NODE_ENV') !== 'prod') {
      console.log({ chatName, numberOfMessages, emails });
      return;
    }

    const emailPromises = emails.map((email) =>
      this.gmailService.sendTemplatedEmail(
        email,
        'You have unread messages',
        'unread-messages-email',
        { chatName, numberOfMessages },
      ),
    );

    await Promise.all(emailPromises);
  }
}
