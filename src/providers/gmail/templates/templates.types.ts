export type HbsTemplateData = {
  'sign-up-email': {
    name: string;
    verificationLink: string;
  };
  'unread-messages-email': {
    chatName: string;
    numberOfMessages: number;
  };
};

export type HbsBulkEmailData<Template extends HbsTemplate> = {
  to: string;
  data: HbsTemplateData[Template];
};

export type HbsTemplate = keyof HbsTemplateData;
