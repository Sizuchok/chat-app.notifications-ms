import { MailerModule } from '@nestjs-modules/mailer';
import { Module } from '@nestjs/common';
import { mailerConfig } from '../../config/gmail-config';
import { GmailService } from './gmail.service';

@Module({
  imports: [
    MailerModule.forRootAsync({
      useFactory: () => {
        return mailerConfig();
      },
    }),
  ],
  providers: [GmailService],
  exports: [GmailService],
})
export class GmailModule {}
