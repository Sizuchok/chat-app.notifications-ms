import { MailerService } from '@nestjs-modules/mailer';
import { Injectable } from '@nestjs/common';
import { HbsTemplate, HbsTemplateData } from './templates/templates.types';

@Injectable()
export class GmailService {
  constructor(private readonly mailerService: MailerService) {}

  public async sendTemplatedEmail<Template extends HbsTemplate>(
    to: string,
    subject: string,
    template: Template,
    data: HbsTemplateData[Template],
  ) {
    await this.mailerService.sendMail({
      to,
      subject,
      template,
      context: data,
    });
  }
}
